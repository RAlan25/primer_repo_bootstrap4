
$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 4000
    });
    $('#exampleModal2').on('show.bs.modal', function (e) {
        console.log("se muestra el modal");
        if($('#suscribirseBtn').hasClass("btn-info")){
            $('#suscribirseBtn').removeClass("btn-info");
            $('#suscribirseBtn').addClass('btn-default');
            $('#suscribirseBtn').prop('disabled',true);
        }
        });
    $('#exampleModal2').on('shown.bs.modal', function (e) {
        console.log("se mostró el modal");
    });
    $('#exampleModal2').on('hide.bs.modal', function (e) {
        console.log("se esconde el modal");
            if ($('#suscribirseBtn').hasClass("btn-default")){
                $('#suscribirseBtn').removeClass("btn-primary");
                $('#suscribirseBtn').addClass('btn-info');
                $('#suscribirseBtn').prop('disabled',false);
            }
    });
    $('#exampleModal2').on('hidden.bs.modal', function (e) {
        console.log("se escondió el modal");
    });
})
